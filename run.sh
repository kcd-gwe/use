#!/bin/bash

# clean up
if [ -x /tmp/kcd-gwe-run ]; then
  rm -rf /tmp/kcd-gwe-run
fi

if [ -x /tmp/kcd-gwe-tpl-tmp ]; then
  rm -rf /tmp/kcd-gwe-tpl-tmp
fi

# create the target folder
mkdir /tmp/kcd-gwe-run

# copy everything from the use repository to the kcd-gwe-run
cp -Rv * /tmp/kcd-gwe-run

# git clone the template repository for temporary usage
git clone git@gitlab.com:kcd-gwe/tpl.git /tmp/kcd-gwe-tpl-tmp

# copy the run script and the templates over to the kcd-gwe-run folder
cp -v /tmp/kcd-gwe-tpl-tmp/iac-terraform.sh /tmp/kcd-gwe-run
cp -Rv /tmp/kcd-gwe-tpl-tmp/tpl /tmp/kcd-gwe-run

# everything is ready, run iac terraform
/tmp/kcd-gwe-run/iac-terraform.sh

